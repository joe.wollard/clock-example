Clock Example
=============

```
Author: Joe Wollard
Date: 2018.07.26
```

Running the Code
----------------

This project was built with parcel-bundler instead of webpack due to the fact that parcel is arguably better suited for projects such as this one where little to no build configurations are needed. However, just like any other project with a build phase, we need to install the dependencies first:

### 1. Installing dependencies

```shell
yarn
```

### 2. Start the server

```shell
yarn start
```

There also exists a `build` script which is included purely for academic purposes. When executed via `yarn build`, it will perform as much treeshaking as is reasonable and writes the deliverable output in the `dist/` directory.
