import {default as SimpleTimeField} from 'react-simple-timefield'
import {InputGroup, InputGroupAddon} from 'reactstrap'
import styles from './sass/main.sass'
import PropTypes from 'prop-types'
import React from 'react'

/**
 * Wraps `react-simple-timefield` to provide a layer of abstraction between it
 * and its use throughout the app. Currently, this means styling it to adhere
 * to Bootstrap4 input groups with a prepended label.
 */
export function TimeField ({label, time, onChange}) {
  return (
    <InputGroup className={styles.TimeField}>
      <InputGroupAddon addonType='prepend'>{label}</InputGroupAddon>
      <SimpleTimeField
        className='form-control'
        placeholder='00:00'
        value={time}
        onChange={onChange}
      />
    </InputGroup>
  )
}

TimeField.propTypes = {
  /** The label to prepend to the input field */
  label: PropTypes.string,
  /** Function to call when the value of the field changes */
  onChange: PropTypes.func,
  /** `HH:mm` formatted time string to use as the default value */
  time: PropTypes.string
}

TimeField.defaultProps = {
  label: 'Time',
  onChange: function () {},
  time: '00:00'
}
