/**
 * Splits a time string in 'HH:mm' format and returns an object with shape
 * `{hour: <HH>, min: <mm>}`
 * @method parseTime
 * @param  {String}  HHmm A time string such as `01:15` or `19:45`
 * @return {Object}  Given `01:15`, the return value will be `{hour: 1, min: 15}`
 */
export function parseTime (HHmm) {
  const parts = HHmm.split(':')
  return {
    hour: parseInt(parts[0]) || 0,
    min: parseInt(parts[1]) || 0
  }
}
