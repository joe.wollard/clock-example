import {parseTime} from './parseTime'

/**
 * Given a start and stop time, this will calculate the number of times a
 * clock's bell will toll assuming that clock will strike the bell at the top
 * of each hour and as many times as represented by the number the hour hand is
 * pointing to.
 *
 * @see parseTime
 *
 * @param  {String}  startTime `HH:mm` representing the start of the time range
 * @param  {String}  endTime `HH:mm` representing the end of the time range
 * @return {Number}  The number of times the bell would strike in the time range
 */
export function calculateTolls (startTime, endTime) {
  const start = parseTime(startTime)
  const end = parseTime(endTime)

  let bellTolls = 0
  let elapsedHours = end.hour - start.hour
  let hour = start.hour

  // The start time was greater than the end time, so if the clock were a pie
  // chart, we'd need to invert the area with which we're concerned.
  if (elapsedHours < 0) {
    elapsedHours += 24
  }

  // Map the zeroth hour to the 12 on the clock face
  hour = hour === 0 ? 12 : hour

  // If the times are identical, calculate tolls based on the full area of the
  // clock (e.g. 24 hours)
  if (start.min === end.min && start.hour === end.hour) {
    elapsedHours = 24
  } else if (start.hour === end.hour && start.min > end.min) {
    // If the hours are the same, but the start minutes are less than the end
    // minutes, the user wants a range of 23 hours
    elapsedHours = 23
  } else if (start.min > 0) {
    // If the start time's minutes are greater than zero, we've missed the toll
    // for that hour and will need to skip it in our calculation
    hour++
    elapsedHours--
  }

  // Starting with `hour`, tally up the number of bell tolls that will be heard
  // within `elapsedHours`
  for (let i = 0; i <= elapsedHours; i++) {
    // Keep `hour` between 1 and 12 since that's what would be on a clock face
    if (hour > 12) {
      hour -= 12
    }
    bellTolls += hour
    hour++
  }
  return bellTolls
}
