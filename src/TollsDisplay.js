import {calculateTolls} from './lib/calculateTolls'
import PropTypes from 'prop-types'
import React from 'react'

/**
 * Displays the number of times a bell would toll in a given time range
 */
export function TollsDisplay ({startTime, endTime}) {
  return (
    <p className='lead'>
      The clock will strike {calculateTolls(startTime, endTime)} times.
    </p>
  )
}

TollsDisplay.propTypes = {
  /** `HH:mm` string representing the start of the time range */
  startTime: PropTypes.string,
  /** `HH:mm` string representing the end of the time range */
  endTime: PropTypes.string
}

TollsDisplay.defaultProps = {
  startTime: '00:00',
  endTime: '00:00'
}
