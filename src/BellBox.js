import {Card, CardHeader, CardBody, Container, Row, Col} from 'reactstrap'
import {TimeField} from './TimeField'
import {TollsDisplay} from './TollsDisplay'
import React, {Component} from 'react'
import styles from './sass/main.sass'

/**
 * A dual purpose component whose jobs it is to track application state (the
 * start and stop times of the time ranges, in this case), and to display the
 * form fields to the user.
 *
 * @NOTE If the app were ever to grow, the application state managed here would
 * likely need to be moved into a global Redux store.
 */
export class BellBox extends Component {
  constructor (props) {
    super(props)
    this.state = {
      startTime: '',
      endTime: ''
    }
  }

  /**
   * Handles value changes from the start and end TimeFields.
   * This simply adds the value to the appropriate key within the component
   * state.
   * @method setTime
   * @param  {String} fieldName One of 'startTime' or 'endTime'
   * @param  {String} newValue  A time string using `HH:mm` format.
   */
  setTime (fieldName, newValue) {
    this.setState({[fieldName]: newValue})
  }

  render () {
    return (
      <Card className={styles.BellBox}>
        <CardHeader>Enter a Time Range</CardHeader>
        <CardBody>
          <Container fluid>
            <Row>
              <Col sm={6} className='text-center'>
                <TimeField
                  label='Start time'
                  onChange={this.setTime.bind(this, 'startTime')}
                  time={this.state.startTime}
                />
              </Col>
              <Col sm={6} className='text-center'>
                <TimeField
                  label='End time'
                  onChange={this.setTime.bind(this, 'endTime')}
                  time={this.state.endTime}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <TollsDisplay
                  startTime={this.state.startTime}
                  endTime={this.state.endTime}
                />
              </Col>
            </Row>
          </Container>
        </CardBody>
      </Card>
    )
  }
}
