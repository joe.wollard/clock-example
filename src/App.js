import {BellBox} from './BellBox'
import {Container, Row, Col} from 'reactstrap'
import React from 'react'

/**
 * A simple component whose job it is to align the content to the center of the
 * viewport.
 */
export function App () {
  return (
    <Container>
      <Row className='align-items-center' style={{height: '100vh'}}>
        <Col className='text-center'>
          <BellBox />
        </Col>
      </Row>
    </Container>
  )
}
