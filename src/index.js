import 'bootstrap/dist/css/bootstrap.min.css'
import {App} from './App.js'
import React from 'react'
import ReactDOM from 'react-dom'

/**
 * Render the <App /> component to the DOM at #app-root
 */
ReactDOM.render(
  <App />,
  document.getElementById('app-root')
)
